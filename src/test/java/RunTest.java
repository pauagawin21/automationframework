import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
    plugin = {"pretty", "html:cucumber-reports/index.html"},
    features = "src/main/resources/features",
    glue = {"glue"},
    monochrome = true
)

@io.cucumber.junit.platform.engine.Cucumber
public class RunTest {
    // @Test
    // public void Test() {

    // }
}
