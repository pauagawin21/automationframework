package glue;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.model.Log;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import automation.config.AutomationFrameworkConfiguration;
import automation.drivers.DriverSingleton;
import automation.pages.CheckOutPage;
import automation.pages.HomePage;
import automation.pages.SignInPage;
import automation.utilities.ConfigurationProperties;
import automation.utilities.Constants;
import automation.utilities.Log4j2;
import automation.utilities.TestCases;
import automation.utilities.Utils;
import io.cucumber.java.*;
import io.cucumber.java.en.*;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@SpringJUnitConfig(classes = AutomationFrameworkConfiguration.class)
public class StepDefinitions {
    // ConfigurationProperties configProp = new ConfigurationProperties();
    private WebDriver driver;

    // Pages
    private HomePage homePage;
    private SignInPage signInPage;
    private CheckOutPage checkOutPage;

    // Extent Test & Report
    ExtentTest test;
    static ExtentReports report = new ExtentReports();
    ExtentSparkReporter spark = new ExtentSparkReporter("reports/TestReport1.html");
    final File CONF = new File("src/main/resources/extentreport.xml");
    // ExtentSparkReporter spark = new ExtentSparkReporter("target/spark/spark.html");
    // spark.loadXMLConfig(CONF);

    TestCases[] tests = TestCases.values();

    @Autowired
    ConfigurationProperties configProp;


    @Before
    public void intializeObjects() throws IOException {
        DriverSingleton.getInstance(configProp.getBrowser());
        homePage = new HomePage();
        signInPage = new SignInPage();
        checkOutPage = new CheckOutPage();
        spark.loadXMLConfig(CONF);

        // Extent Report
        report.attachReporter(spark);
        // TestCases[] tests = TestCases.values();
        test = report.createTest(tests[Utils.testCounter].getTestName());

        // Log4j
        Log4j2.getLogData(Log.class.getName());
        Log4j2.startTest(tests[Utils.testCounter].getTestName());
        // Utils.testCounter++;
    }

    @Given ("I go to the Website")
    public void i_go_to_the_website() {
        System.out.println("user is on home page");
        driver = DriverSingleton.getDriver();
        driver.get(Constants.URL);

        // Extent
        test.log(Status.PASS, "Navigating to " + Constants.URL);

        // Log4j
        Log4j2.info("Navigating to " + Constants.URL);

    }

    @When("I click on Sign In button")
    public void i_click_on_sign_in_button() {
        homePage.clickSignIn();

        // Extent
        test.log(Status.PASS, "Sign in button is working.");

        // Log4j
        Log4j2.info("Sign in button is working.");
    }

    @When("I add two elements to the cart")
    public void i_add_two_elements_to_the_cart() {
        homePage.addFirstItemToCart();
        homePage.addSecondItemToCart();

        // Extent
        test.log(Status.PASS, "Two elements were added to the cart");

        // Log4j
        Log4j2.info("Sign in button is working.");
    }

    @And("I specify my credentials and click Login")
    public void i_specify_my_credentials_and_click_login() {
        signInPage.logIn(configProp.getEmail(), configProp.getPassword());

        // Extent
        test.log(Status.PASS, "Login is working.");

        // Log4j
        Log4j2.info("Login is working.");
    }

    @And("I proceed to checkout")
    public void i_proceed_to_checkout() {
        checkOutPage.goToCheckout();

        // Extent
        test.log(Status.PASS, "Proceeded to checkout.");

        // Log4j
        Log4j2.info("Proceeded to checkout.");
    }

    @And("I confirm address, shipping, payment and final order")
    public void i_confirm_address_shipping_payment_and_final_order() {
        signInPage.logIn(configProp.getEmail(), configProp.getPassword());
        checkOutPage.confirmAddress();
        checkOutPage.confirmShipping();
        checkOutPage.confirmPaymentMethod();
        checkOutPage.confirmPayment();

        // Log4j
        Log4j2.info("System confirmed user's address,shipping, payment method and final order.");
    }

    @Then("I can log into the website")
    public void i_can_log_into_the_website() {
        if (configProp.getUsername().equals(homePage.getUsername())) {
            // Extent
            test.log(Status.PASS, "The authentication is successful.");

            // Log4j
            Log4j2.info("The authentication is successful.");
            Log4j2.endTest(tests[Utils.testCounter].getTestName());
            Utils.testCounter++;
        }
        else {
            // Extent
            test.log(Status.FAIL, "The authentication is not successful.");

            // Log4j
            Log4j2.error("The authentication is not successful.");
        }
        // Write code here that turns the phrase above into concrete actions
    }

    @Then("The elements are bought")
    public void the_elements_are_bought() {
        if(checkOutPage.checkOrderStatus()) {
            // Extent
            test.log(Status.PASS, "System confirmed two items are bought.");

            // Log4j
            Log4j2.info("System confirmed two items are bought.");
            Log4j2.endTest(tests[Utils.testCounter].getTestName());

        }
        else {
            // Extent
            test.log(Status.FAIL, "The two items were not bought.");

            // Log4j
            Log4j2.error("The two items were not bought.");
        }

        assertTrue(checkOutPage.checkOrderStatus());
        // Write code here that turns the phrase above into concrete actions
    }

    @After
    public void closeObjects() {
        report.flush();
        DriverSingleton.closeObjectInstance();
    }

}
