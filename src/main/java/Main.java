import org.openqa.selenium.WebDriver;

import automation.drivers.DriverSingleton;
import automation.pages.CheckOutPage;
import automation.pages.HomePage;
import automation.pages.SignInPage;
import automation.utilities.Constants;
import automation.utilities.FrameworkProperty;

public class Main {
    public static void main(String[] args) {
        FrameworkProperty frameworkProp = new FrameworkProperty();
        DriverSingleton driverSingleton = DriverSingleton.getInstance(frameworkProp.getProperty("browser"));
        WebDriver driver = driverSingleton.getDriver();
        driver.get("http://automationpractice.com/index.php");

        HomePage homePage = new HomePage();
        homePage.getUsername();
        homePage.addFirstItemToCart();
        homePage.addSecondItemToCart();

        CheckOutPage checkOutPage = new CheckOutPage();
        checkOutPage.goToCheckout();

        SignInPage signInPage = new SignInPage();
        signInPage.logIn(frameworkProp.getProperty(Constants.EMAIL), frameworkProp.getProperty(Constants.PASSWORD));

        checkOutPage.confirmAddress();
        checkOutPage.confirmShipping();
        checkOutPage.confirmPaymentMethod();
        checkOutPage.confirmPayment();
        if(checkOutPage.checkOrderStatus()) {
            System.out.println("Test Case Complete");
        }
        // checkOutPage.checkOrderStatus();

    }
}
