package automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.drivers.DriverSingleton;
import automation.utilities.Utils;

public class SignInPage {

    private WebDriver driver;

    public SignInPage() {
        this.driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
    }


    // Use annotation @Findby to look for web elements
    // Login section
    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "passwd")
    private WebElement passwordField;

    @FindBy(id = "SubmitLogin")
    private WebElement signInButton;


    // Create user section
    @FindBy(id = "email_create")
    private WebElement createAccountField;

    @FindBy(id = "SubmitCreate")
    private WebElement createAccountButton;


    // Create method/function
    // Login method
    public void logIn(String email, String password) {
        emailField.sendKeys(email);
        passwordField.sendKeys(Utils.decode64(password));
        signInButton.click();
    }

    // Signup method
    public void signUp(String email) {
        createAccountField.sendKeys(email);
        createAccountButton.click();
    }


}
