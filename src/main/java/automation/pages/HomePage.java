package automation.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import automation.drivers.DriverSingleton;
import automation.utilities.Constants;
import automation.utilities.Utils;

public class HomePage {

    private WebDriver driver;

    public HomePage() {
        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
    }

    // Search Elements
    @FindBy(id = "search_query_top")
    private WebElement searchField;

    @FindBy(css = "#searchbox > button")
    private WebElement searchButton;

    @FindBy(css = "#center_column > ul > li > div > div.left-block > div > a.product_img_link > img")
    private WebElement searchResult;


    // SignIn Elements
    @FindBy(css = "#header > div.nav > div > div > nav > div.header_user_info > a")
    private WebElement signInButton;

    @FindBy(css = "#header > div.nav > div > div > nav > div:nth-child(1) > a")
    private WebElement username;



    @FindBy(css = "#homefeatured > li.ajax_block_product.col-xs-12.col-sm-4.col-md-3.first-in-line.first-item-of-tablet-line.first-item-of-mobile-line")
    private WebElement firstItemHover;

    @FindBy(css = "#homefeatured > li:nth-child(2)")
    private WebElement secondItemHover;

    @FindBy(css = "#homefeatured > li.ajax_block_product.col-xs-12.col-sm-4.col-md-3.first-in-line.first-item-of-tablet-line.first-item-of-mobile-line > div > div.right-block > div.button-container > a.button.ajax_add_to_cart_button.btn.btn-default")
    private WebElement addCartFirstButton;

    @FindBy(css = "#homefeatured > li:nth-child(2) > div > div.right-block > div.button-container > a.button.ajax_add_to_cart_button.btn.btn-default")
    private WebElement addCartSecondButton;

    @FindBy(css = "#header > div:nth-child(3) > div > div > div:nth-child(3) > div > a")
    private WebElement viewCartButton;

    @FindBy(css = "#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6 > div.button-container > span")
    private WebElement continueShopButton;

    @FindBy(css = "#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6 > div.button-container > a")
    private WebElement proceedShopButton;

    public void clickSignIn() {
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(signInButton));

        signInButton.click();
    }

    public String getUsername() {
        return username.getText();
    }

    public void addFirstItemToCart() {

        Actions hover = new Actions(driver);
        hover.moveToElement(firstItemHover).build().perform();

        addCartFirstButton.click();

        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(continueShopButton));

        continueShopButton.click();

        if (viewCartButton.getText().contains(Constants.CART_QUANTITY)) {
            System.out.println("Cart has been updated.");
        }
        else {
            System.out.println("Cart has not been updated.");
            Utils.takeScreenshots();
        }
    }

    public void addSecondItemToCart() {

        Actions hover = new Actions(driver);
        hover.moveToElement(secondItemHover).build().perform();

        addCartSecondButton.click();

        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(proceedShopButton));
        proceedShopButton.click();
    }

    public Boolean searchElements(String searchStr) {
        searchField.sendKeys(searchStr);
        searchButton.click();

        try {
            if (searchResult.isEnabled()) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }

        return false;
    }

}
