package automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import automation.drivers.DriverSingleton;
import automation.utilities.Constants;

public class CheckOutPage {

    private WebDriver driver;

    public CheckOutPage() {
        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
    }


    // Page Validator
    @FindBy(css = "head > title")
    private WebElement titlePage;


    @FindBy(id = "summary_products_quantity")
    private WebElement cartQuantity;


    @FindBy(css = "#center_column > p.cart_navigation.clearfix > a.button.btn.btn-default.standard-checkout.button-medium")
    private WebElement confirmItemButton;

    @FindBy(css = "#center_column > form > p > button")
    private WebElement confirmAddressButton;

    @FindBy(css = "#cgv")
    private WebElement agreeShippingCheckBox;

    @FindBy(css = "#form > p > button")
    private WebElement confirmShippingButton;

    @FindBy(css = "#HOOK_PAYMENT > div:nth-child(1) > div > p > a")
    private WebElement payByBankWireOption;

    @FindBy(css = "#cart_navigation > button")
    private WebElement confirmPaymentButton;

    @FindBy(css = "#center_column > div > p > strong")
    private WebElement orderConfirmMessage;


    // Checkout Methods
    // Check if title page is true
    public Boolean checkTitle(String title) {
        return titlePage.getText().equals(title);
    }


    public String summaryCartItemQuantity() {
        return cartQuantity.getText();
    }


    public void goToCheckout() {
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(confirmItemButton));
        confirmItemButton.click();
    }

    public void confirmAddress() {
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(confirmAddressButton));
        confirmAddressButton.click();
    }

    public void confirmShipping() {
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(confirmShippingButton));
        agreeShippingCheckBox.click();
        confirmShippingButton.click();
    }

    public void confirmPaymentMethod() {
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(payByBankWireOption));
        payByBankWireOption.click();
    }

    public void confirmPayment() {
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(confirmPaymentButton));
        confirmPaymentButton.click();
    }

    public Boolean checkOrderStatus() {
        return orderConfirmMessage.getText().contains(Constants.COMPLETE_ORDER);
    }

}
