package automation.drivers;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import automation.drivers.strategies.DriverStrategy;
import automation.drivers.strategies.DriverStrategyImplementer;

public class DriverSingleton {

    private static DriverSingleton instance = null;
    private static WebDriver driver;

    public DriverSingleton(String driver) { instantiate(driver); }

    public WebDriver instantiate(String strategy) {
        DriverStrategy driverStrategy = DriverStrategyImplementer.chooseStrategy(strategy);
        driver = driverStrategy.setStrategy();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        return driver;
    }

    public static DriverSingleton getInstance(String driver) {
        if (instance == null) {
            instance = new DriverSingleton(driver); // To call singleton driver
        }

        return instance;
    }

    // Method to close instantiate and prevent memory leaks and driver running
    public static void closeObjectInstance() {
        instance = null;
        driver.quit();
    }

    public static WebDriver getDriver() {
        return driver;
    }

}
