package automation.drivers.strategies;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class PhantomJs implements DriverStrategy {


    public WebDriver setStrategy() {
        System.setProperty("phantom.binary.path", "src/main/resources/phantomjs");
        DesiredCapabilities desiredCap = new DesiredCapabilities();
        desiredCap.setJavascriptEnabled(true);
        WebDriver driver = new PhantomJSDriver(desiredCap);

        return driver;

        // return null;
    }

}
