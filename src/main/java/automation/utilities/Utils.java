package automation.utilities;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.Random;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.springframework.util.FileCopyUtils;

import automation.drivers.DriverSingleton;

public class Utils {

    public static int testCounter = 0;

    // Method to decode an encoded base64 string.
    public static String decode64(String encodedStr) {
        Base64.Decoder decoder = Base64.getDecoder();
        return new String(decoder.decode(encodedStr.getBytes()));
    }


    // Take Screenshots
    public static boolean takeScreenshots() {
        File file = ((TakesScreenshot) DriverSingleton.getDriver()).getScreenshotAs(OutputType.FILE);
        try {
            FileCopyUtils.copy(file, new File(Constants.SCREENSHOTS_DIR + generateRandomString(Constants.SCREENSHOT_NAME_LEN) + Constants.SCREENSHOTS_FORMAT));
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private static String generateRandomString(int length) {
        String seedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder sb = new StringBuilder();
        int i = 0;
        Random random = new Random();
        while (i < length) {
            sb.append(seedChars.charAt(random.nextInt(seedChars.length())));
            i++;
        }
        return sb.toString();
    }
}
