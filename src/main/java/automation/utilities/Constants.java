package automation.utilities;

public class Constants {

    // Int constants
    public static final int TIMEOUT = 15;
    public static final int SCREENSHOT_NAME_LEN = 7;

    // String constants
    public static final String URL = "http://automationpractice.com/index.php";

    public static final String PROP_FILE_NAME = "framework.properties";
    public static final String FILE_NOT_FOUND_EXCEPTION = "The Property file has not been found.";
    public static final String CHROME = "Chrome";
    public static final String FIREFOX = "Firefox";
    public static final String PHANTOMJS = "PhantomJs";
    public static final String CART_QUANTITY = "1 Product";
    public static final String COMPLETE_ORDER = "Your order on My Store is complete.";
    public static final String CART_QUANTITY_TEST = "2 Products";
    public static final String SEARCH_ITEM_TEST = "blouse";

    public static final String BROWSER = "browser";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String USERNAME = "uname";

    public static final String SCREENSHOTS_DIR = "screenshots/";
    public static final String SCREENSHOTS_FORMAT = ".png";

}
