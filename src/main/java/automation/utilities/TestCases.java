package automation.utilities;

public enum TestCases {
    T1("Testing the authenthication"),
    T2("Testing the purchase of two items");

    private String testName;

    TestCases(String value) {
        this.testName = value; // Gets value of the T1 and T2.
    }

    public String getTestName() {
        return testName;
    }
}
