package automation.utilities;

import java.util.Base64;
import java.util.Scanner;

public class Base64Encoder {

    public String decodeStr;

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("\n===> ENCODE TO BASE64 FORMAT <===\n\nEnter Password to encode: ");
        String passwordString = input.next();

        Base64Encoder encodePassword = new Base64Encoder();
        encodePassword.encode64(passwordString);

        input.close();
    }

    public void encode64(String decodeStr) {
        Base64.Encoder encoder = Base64.getEncoder();
        System.out.println("\nEncoded Password: " + encoder.encodeToString(decodeStr.getBytes()) + "\n\n");
        System.out.println("*** USE THE ENCODED PASSWORD AS A VALUE TO THE KEY (password) IN THE \"resources>framework.properties\" file. ***\n\n");

    }



}
